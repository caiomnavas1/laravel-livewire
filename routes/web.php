<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Livewire\{
    ShowTeets,
};

use App\Http\Livewire\User\UploadAvatar;

Route::get('/avatar', UploadAvatar::class)->middleware('auth')->name('upload.avatar');
Route::get('/tweets', ShowTeets::class)->middleware('auth')->name('tweets.index');

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
