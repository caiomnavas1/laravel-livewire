<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Upload Avatar') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form action="#" method="post" wire:submit.prevent="storeAvatar" enctype="multipart/form-data">
                    @if ($avatar)
                        Photo Preview:
                        <img src="{{ $avatar->temporaryUrl() }}">
                    @endif
                    <input type="file" name="avatar" id="avatar" wire:model="avatar"><br>
                    @error('avatar') {{ $message }} @enderror<br>
                    <button type="submit" class="bg-gray-100">Enviar Imagem</button>
                </form>
            </div>
        </div>
    </div>
</div>
