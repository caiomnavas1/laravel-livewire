<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Show Tweets') }}
    </h2>
</x-slot>
<div>
<div class="pt-8 pb-3">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                <form method="post" wire:submit.prevent="create">
                    <textarea name="content" id="content" cols="20" class="w-full" wire:model="content"></textarea>
                    @error('content') {{ $message }} @enderror<br>
                    <button type="submit" class="bg-indigo-50 px-6 py-2">Tweetar</button>
                </form>
            </div>
        </div>
    </div>
</div>

@forelse($tweets as $tweet)
        <div class="py-2">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-2 bg-white border-b border-gray-200">
                        <div class="flex">
                            <div class="w-auto">
                                @if(!$tweet->user->avatar)
                                    <img src="{{ url("images/no-image.jpg") }}" alt="{{ $tweet->user->name }}" class="rounded h-10 w-10 mr-2" style="display: unset;">
                                @else
                                    <img src="{{ url("storage/{$tweet->user->avatar}") }}" alt="{{ $tweet->user->name }}" class="rounded h-10 w-10 mr-2" style="display: unset;">
                                @endif
                            </div>
                            <div class="w-full">
                                <strong>{{ $tweet->user->name }}</strong><br>
                                {{ $tweet->content }}
                            </div>
                        </div>
                        @if($tweet->likes->count())
                            <a href="#" wire:click.prevent="unlike({{ $tweet->id }})" class="text-red-600">Descurtir</a> - {{ $tweet->likes->count() }} curtida(s)
                        @else
                            <a href="#" wire:click.prevent="like({{ $tweet->id }})" class="text-green-600">Curtir</a> - {{ $tweet->likes->count() }} curtida(s)
                        @endif
                        <br>
                    </div>
                </div>
            </div>
        </div>
@empty
        <div class="py-2">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-2">
                        Sem Tweets para exibir
                    </div>
                </div>
            </div>
        </div>
@endforelse
    @if($tweets->count())
        <div class="py-2">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                    <div class="p-2">
                        {{ $tweets->links() }}
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
