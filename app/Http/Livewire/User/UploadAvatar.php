<?php

namespace App\Http\Livewire\User;

use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithFileUploads;


class UploadAvatar extends Component
{
    use WithFileUploads;

    public $avatar;

    public function render()
    {
        return view('livewire.user.upload-avatar');
    }

    public function storeAvatar()
    {
        $this->validate([
            'avatar' => 'required|image|max:1024'
        ]);

        $user = auth()->user();

        $nameFile = Str::slug($user->name).'.'.$this->avatar->getClientOriginalExtension();

        if($path = $this->avatar->storeAs('avatar', $nameFile)){
            $user->update([
                'avatar' => $path,
            ]);
        }

        return redirect()->route('tweets.index');
    }
}
